package jp.cmscom.c2.transform.msoffice;

public enum DocPart {
	Properties,
	Header,
	Footer,
	Notes,
	Contents;
}